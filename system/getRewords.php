  <?php
    /*
     * Достижения и их награды.
     * Типы наград:
     * exp - опыта:кол-во
     * coins - монеты:кол-во
     * setGroup - выдать привилегию:группа/время (в UNIX)
     * slots - добавить слоты:кол-во
     */
    private $achievements=[
        'Welcome'=>'exp:15,coins:100,setGroup:Deluxe/86400'
    ];

    public function __construct () {
        $this->GUser=(new GUser());
    }

    public function getRewords($achievements, $update=[]) {
        $isx=[];$sx=[];$isx1=[];$sx1=[];$up=0;
        if (strripos($this->achievements[$achievements], ',')) {
            $items = explode(',', $this->achievements[$achievements]);
            foreach ($items as $val) {
                $isx[] = explode(':', $val);
            }
            foreach ($isx as $value) {
                if( strripos($value[1], '/') ) {
                    $strArray=explode('/', $value[1]);
                } else $strArray=$value[1];
                $sx[$value[0]] = $strArray;

                if( !empty($update) ) {
                    if( isset($update[$value[0]]) ) {
                        if (strripos($update[$value[0]], ',')) {
                            $data = explode(',', $update[$value[0]]);
                            foreach ($data as $valData) {
                                $isx1[] = explode(':', $valData);
                            }
                            foreach ($isx1 as $valData2) {
                                $sx1[$valData2[0]] = $valData2[1];
                            }
                        } elseif (strripos($update[$value[0]], ':')) {
                            $sx1[explode(':', $update[$value[0]])[0]] = explode(':', $update[$value[0]])[1];
                        }
                        $key=$value[0];
                        if( !array_key_exists('usertable', $sx1) ) $usertable='login'; else $usertable=$sx1['usertable'];
                        if( !array_key_exists('column', $sx1) ) $column=$key; else $column=$sx1['column'];

                        if( array_key_exists('table', $sx1) ) {
                            $table=$this->StrArray($sx1['table']);
                            $column=$this->StrArray($column);
                            $usertable=$this->StrArray($usertable);

                            if( is_array($table) ) {
                                if( !is_array($column) ) return $this->FoundDevelopersError('Need a second column.', '63', 'Achievements.php', __NAMESPACE__);
                                $data=$sx[$value[0]];
                                foreach ($table as $key=>$dTable) {

                                    $this->GUser->UpdatePex();
                                    if( $this->GUser->PexUUID() != $this->GUser->uuidConvert($this->GUser->Login()) && $this->GUser->PexGroup() != $data[0] ) {
                                        self::db()->query(self::SQLInsert('4'), $dTable, 'login', 'uuid', 'group', 'timeout', $this->GUser->Login(), $this->GUser->uuidConvert($this->GUser->Login()), $data[0], time()+$data[1]);
                                        $up+=1;
                                        continue;
                                    }
                                    $this->GUser->UpdateInheritance();
                                    if( empty($this->GUser->PermissionInheritance()) ) {
                                        self::db()->query(self::SQLInsert('3'), $table[$key+1], 'child', 'parent', 'type', $this->GUser->uuidConvert($this->GUser->Login()), $data[0], '1');

                                        $up+=1;
                                        continue;
                                    }
                                    if( $up == 0 ) {
                                        if( !is_array($usertable) ) return $this->FoundDevelopersError('Need a second usertable name.', '82', 'Achievements.php', __NAMESPACE__);
                                        self::db()->query('UPDATE ?n SET ?n=?n+?s WHERE ?n=?s', $dTable, $column[$key], $column[$key], $sx[$value[0]][1], $usertable[$key], $this->GUser->Login());
//                                        print_r($sx[$value[0]]);
                                    }
                                }
                            } else {
                                if( is_array($column) ) return $this->FoundDevelopersError('Need a second table.', '67', 'Achievements.php', __NAMESPACE__);
                                self::db()->query('UPDATE ?n SET ?n=?n+?s WHERE ?n=?s', $table, $column, $column, $sx[$value[0]], $usertable, $this->GUser->Login());
                            }
                        } else return $this->FoundDevelopersError('No table specified.', '47', 'Achievements.php', __NAMESPACE__);

                    }
                }
            }
        } elseif (strripos($this->achievements[$achievements], ':')) {
            $sx[explode(':', $this->achievements[$achievements])[0]] = explode(':', $this->achievements[$achievements])[1];
        }



//        return $sx;
    }
    /*
    print_r((new \Base\Rewords\Achievements())->getRewords('Welcome', [
      'exp'=>'table:levelSystem',
      'coins'=>'table:iConomy,usertable:username,column:balance',
      'setGroup'=>'table:[pex|permissions_inheritance],column:[timeout|reword],usertable:[login|child]',
    ]));
    */