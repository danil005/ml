<?php

namespace Base\ML;

class Alphabet {

    public function getMsgGreeting($messages) {
        $array=[
            'привет',
            'приветик',
            'здравствуйте',
            'здравствуй',
            'хей',
            'салам алейкум',
            'hi',
            'хай',
            'псс привет',
            'опа привет',
            'здарова',
            'приветсвую',
            'приветсвую вас о мой господин',
            'дарова',
            'привет бот',
            'привет робот',
            'привет андройд',
            'слышь',
            'слыш',
            'эй инопланетянин',
            'халло',
            'хало',
            'привет даниэль',
            'прив',
            'привэт',
            'привээт',
            'привээт',
            'приивэээт',
            'приииивэт',
            'приивээт',
            'привеееет',
            'привки',
            'привееетик',
            'привеееееетик',
            'прииивет',
            'при-вет',
            'о привет',

        ];

        if( count($_SESSION['rml']['Greeting']) > 4 ) {
            unset($_SESSION['rml']['Greeting']);
            return "Tired";
        }

        if( in_array($this->Convert($messages), $array) ) {
            $this->WriteW($this->Convert($messages), 'Greeting');
            return true;
        }
        return false;
    }

    public function Greeting($type) {
        $array=[];
        $type=$this->Convert($type);

        if( in_array($type, ['здравствуйте','здравствуй']) ) $array=[
            'Здравствуйте.',
            'Рад приветствовать.',
            'Здравствуй, какими судьбами?',
        ];
        elseif( in_array($type, ['салам алейкум']) ) $array=[
            'ас-салям'
        ];
        elseif( in_array($type, ['привет бот', 'привет робот', 'привет андройд']) ) $array=[
            'Между прочем, я не бот, я высокоразветое существо, как люди. :3',
            'Знаешь, это уже перебор, я не хочу с тобой разговаривтать... Хотя кого я обамнываю, но мне все равно обидно!',
            'Я НЕ БОТ!',
            'Привет, но не называй меня ботом или еще кем-то, хорошо?'
        ];
        elseif( in_array($type, ['привет даниэль']) ) $array=[
            'О, привет, неужели меня кто-то назвал по имени!',
            'Привет, рад тебя видеть.',
        ];
        elseif( in_array($type, ['эй инопланетянин']) ) $array=[
            'Теперь мы доказали, что инопланетные расы существуют.',
            'Ну хоть ты это заметил.',
            'Тсс, а то заберу и отправлю на корм рыбам.'
        ];
        elseif( in_array($type, ['привэт','привээт','привээт','приивэээт','приииивэт','приивээт']) ) $array=[
            'Привэээт'
        ];
        else
            $array=[
                'Привет.',
                'Приветсвую тебя, друг.',
                'Привет, давно не видились.',
                'Ну, привет.',
            ];
        $response=$array[rand(0,count($array)-1)];

        return $response;
    }

    public function getConsentMsg($messages) {
        $array=[

        ];

        if( count($_SESSION['rml']['Consent']) > 4 ) {
            unset($_SESSION['rml']['Consent']);
            return "Tired";
        }

        if( in_array($this->Convert($messages), $array) ) {
            $this->WriteW($this->Convert($messages), 'Consent');
            return true;
        }
        return false;
    }

    public function DoNotUnderstand() {
        $array=[
            'Прости, но я тебя не понимаю.',
            'Я не знаю, что сказать тебе.',
            'Есть некоторая проблема, я еще не знаю, что ты имеешь введу.',
            'Мой создатель еще не сказал, как мне отвечать на твое сообщение.'
        ];
        $response=$array[rand(0,count($array)-1)];

        return $response;
    }

    public function Tired() {
        $array=[
            'Слушай, тебе не надоело говорить одно и тоже?',
            'Может сменим тему?',
            'Ну, вообще то я не только приветствовать умею.',
            'Если что я тут создан не для того, чтобы тупо приветствовать!',
            'Наверно это будет странно, но тебе не надоело ли? Мне вот - да.',
            'Я Даниэль Людвик IV! Я все понял с первого раза! Жаль, что я не лорд. :c',
            'Объясни мне, как долго ты собираешься мучать меня одним и тем же?',

        ];
        $response=$array[rand(0,count($array)-1)];

        return $response;
    }

    private function WriteW($data, $type) {
        if( isset($_SESSION['rml']) ) {
            if( isset($_SESSION['rml'][$type]) ) {
                $cSession=count($_SESSION['rml'][$type]);
            } else $cSession=0;
        } else {
            $_SESSION['rml']=[];
            $cSession=0;
        }
        for($i=0;$i<=$cSession;$i++) {
            if( empty($_SESSION['rml'][$type][$i]) ) {
                $_SESSION['rml'][$type][$i]=$data;
                break;
            }
        }
    }

    public function Convert($text) {
        preg_match_all("/[\w\s\d]+/u", $text, $result);
        $textClear = implode('', $result[0]);
        $textClear=preg_replace("/ {2,}/"," ",$textClear);
        return mb_strtolower($textClear);
    }

}